CREATE DATABASE News
GO
USE [News]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 23/05/2024 03:09:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](100) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 23/05/2024 03:09:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Country] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (1, N'ALL', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (2, N'Business', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (3, N'Entertainment', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (4, N'General', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (5, N'Health', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (6, N'Science', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (7, N'Sports', 1)
GO
INSERT [dbo].[Category] ([Id], [Category], [IsActive]) VALUES (8, N'Technology', 1)
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON 
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (1, N'ae', N'United Arab Emirates', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (2, N'ar', N'Argentina', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (3, N'at', N'Austria', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (4, N'au', N'Australia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (5, N'be', N'Belgium', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (6, N'bg', N'Bulgaria', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (7, N'br', N'Brazil', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (8, N'ca', N'Canada', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (9, N'ch', N'Switzerland', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (10, N'cn', N'China', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (11, N'co', N'Colombia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (12, N'cu', N'Cuba', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (13, N'cz', N'Czechia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (14, N'de', N'Germany', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (15, N'eg', N'Egypt', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (16, N'fr', N'France', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (17, N'gb', N'Inggris', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (18, N'gr', N'Greece', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (19, N'hk', N'Hongkong', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (20, N'hu', N'Hungary', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (21, N'id', N'Indonesia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (22, N'ie', N'Iraq', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (23, N'il', N'Israel', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (24, N'in', N'India', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (25, N'it', N'Italy', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (26, N'jp', N'Jepang', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (27, N'kr', N'Korea', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (28, N'lt', N'Lithuania', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (29, N'lv', N'Latvia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (30, N'ma', N'Morocco', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (31, N'mx', N'Mexico', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (32, N'my', N'Malaysia', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (33, N'ng', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (34, N'nl', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (35, N'no', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (36, N'nz', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (37, N'ph', N'Philipina', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (38, N'pl', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (39, N'pt', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (40, N'ro', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (41, N'rs', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (42, N'ru', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (43, N'sa', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (44, N'se', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (45, N'sg', N'Singapura', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (46, N'si', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (47, N'sk', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (48, N'th', N'Thailand', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (49, N'tr', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (50, N'tw', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (51, N'ua', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (52, N'us', N'Amerika', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (53, N've', N'', 1)
GO
INSERT [dbo].[Country] ([Id], [Country], [Description], [IsActive]) VALUES (54, N'za', N'', 1)
GO
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
/****** Object:  StoredProcedure [dbo].[SP_GetCategory]    Script Date: 23/05/2024 03:09:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetCategory]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Category, Category FROM Category WHERE IsActive = 1 ORDER BY Id ASC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetCountry]    Script Date: 23/05/2024 03:09:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetCountry]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Country, Description FROM Country WHERE IsActive = 1 AND Description != '' ORDER BY Country ASC
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetNews]    Script Date: 23/05/2024 03:09:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[SP_GetNews]  
(
@country as varchar(max),
@category as varchar(max),
@query as varchar(max),
@page as varchar(max)
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

	IF @category = 'ALL'
	BEGIN
		SET @category = ''
	END
  
	Declare @url as varchar(max);

	SET @url = 'https://newsapi.org/v2/top-headlines?country=' + @country + '&category=' + @category + '&q=' + @query + '&page=' + @page + '&pageSize=20&apiKey=7e3ed7bef61a409793c888e00c022c3a'

	print @url

	Declare @Object as Int;    

	DECLARE @responseText as NVARCHAR(max);

	Declare @Response table(responseText text);

	DECLARE @value as NVARCHAR(max);

	DECLARE @points as NVARCHAR(max);

	DECLARE @value2 as NVARCHAR(max);

	DECLARE @value3 as NVARCHAR(max);

	EXEC sp_OACreate 'MSXML2.XMLHTTP', @Object OUT;

	EXEC sp_OAMethod @Object, 'open', NULL, 'get', @url, 'false'

	EXEC sp_OAMethod @Object, 'send'

	INSERT INTO @Response EXEC sp_OAGetProperty @Object, 'responseText'

	Select @responseText = responseText from @Response

	SELECT @value = [value] FROM OpenJson(@responseText) WHERE [key] = 'articles'

	declare @source varchar(max)
	declare @author varchar(max)
	declare @title varchar(max)
	declare @description varchar(max)
	declare @urlnews varchar(max)
	declare @urltoimage varchar(max)
	declare @published varchar(max)
	declare @tableHTML varchar(Max)
	declare @loop int
	declare @HitungRow int

	create table #Temp
	(
		source Varchar(max), 
		author Varchar(max), 
		title Varchar(max), 
		description Varchar(max), 
		url Varchar(max), 
		urlimage Varchar(max), 
		published Varchar(max)
	)

	set @HitungRow=(select Count(*) from OpenJson(@value))

	IF @HitungRow > 0

	BEGIN
		Set @loop=1

		while @loop<=@HitungRow
		BEGIN

			SELECT @points = [value] FROM OpenJson(@value) WHERE [key] = @loop

			SET @source = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'source')
			SET @author = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'author')
			SET @title = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'title')
			SET @description = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'description')
			SET @urlnews = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'url')
			SET @urltoimage = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'urlToImage')
			SET @published = (SELECT [value] FROM OpenJson(@points) WHERE [key] = 'publishedAt')

			INSERT INTO #Temp

			SELECT (SELECT [value] FROM OpenJson(@source) WHERE [key]='name') Source, @author Author, @title Title, @description Description, @urlnews Url, @urltoimage UrlImage, DATEADD(HOUR,7,REPLACE(REPLACE(@published,'T',' '),'Z','')) Published

		set @loop=@loop+1

		END
	END
	
	SELECT source,ISNULL(author,'') author,title,description,url,urlimage, published FROM #Temp WHERE source != '[Removed]'
	ORDER BY published DESC

	DROP TABLE #Temp
END  
GO
